package com.example.particle;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class ParticleDBHelper extends SQLiteOpenHelper {

// ....

    public ParticleDBHelper(Context context)  {
        super(context, "particleDB", null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        // Script to create table.
        String script = "CREATE TABLE " + "data" + "("
                + "id" + " INTEGER PRIMARY KEY," + "pm25" + " TEXT,"
                + "pm10" + " TEXT," + "gps" + " TEXT," + "timestamp" + " TEXT" + ")";
        // Execute script.
        db.execSQL(script);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // Drop table
        db.execSQL("DROP TABLE IF EXISTS " + "data");


        // Recreate
        onCreate(db);
    }

    public void clear() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("data", null, null);
    }

    public void addData(DataDB data) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("pm25", data.pm25);
        values.put("pm10", data.pm10);
        values.put("gps", data.gps);
        values.put("timestamp", data.timestamp);

        // Inserting Row
        db.insert("data", null, values);

        // Closing database connection
        db.close();
    }

    public List<DataDB> getAllMesurements() {
        List<DataDB> list = new ArrayList<DataDB>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + "data" + " ORDER BY timestamp DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                DataDB data = new DataDB();
                data.pm25 = cursor.getString(1);
                data.pm10 = cursor.getString(2);
                data.gps = cursor.getString(3);
                data.timestamp = cursor.getString(4);

                // Adding note to list
                list.add(data);
            } while (cursor.moveToNext());
        }
        // return note list
        return list;
    }
}