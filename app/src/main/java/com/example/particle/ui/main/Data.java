package com.example.particle.ui.main;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.particle.DataDB;
import com.example.particle.MainActivity;
import com.example.particle.ParticleDBHelper;
import com.example.particle.R;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.DOWNLOAD_SERVICE;

/**
 * A placeholder fragment containing a simple view.
 */
public class Data extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    // #defines for identifying shared types between calling functions
    private final static int SENSOR_STATUS = 2; // used in bluetooth handler to identify message status

    private PageViewModel pageViewModel;

    private ListView mListView;
    private ArrayAdapter<DataDB> mListAdapter;

    public static Data newInstance(int index) {
        Data fragment = new Data();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    // This event fires 1st, before creation of fragment or any views
    // The onAttach method is called when the Fragment instance is associated with an Activity.
    // This does not mean the Activity is fully initialized.
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        pageViewModel.setIndex(index);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_data, container, false);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListView = (ListView)view.findViewById(R.id.dataList);
        mListAdapter = new ArrayAdapter<DataDB>(getActivity(), android.R.layout.simple_list_item_1);
        mListView.setAdapter(mListAdapter); // assign model to view

        Button bRefresh = (Button)view.findViewById(R.id.buttonRefresh);
        bRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                loadDb();
            }
        });

        Button bErase = (Button)view.findViewById(R.id.buttonErase);
        bErase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                ParticleDBHelper db = new ParticleDBHelper(getActivity());
                db.clear();
                loadDb();
            }
        });

        Button bExport = (Button)view.findViewById(R.id.buttonExport);
        bExport.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(final View v) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},1);
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},1);
                String csv = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/data.csv";

                try {
                    CSVWriter writer = new CSVWriter(new FileWriter(csv));
                    ParticleDBHelper db = new ParticleDBHelper(getActivity());

                    List<String[]> data = new ArrayList<String[]>();
                    data.add(new String[] {"pm25", "pm10", "timestamp", "location"});
                    for(DataDB e : db.getAllMesurements())
                        data.add(new String[] {e.pm25, e.pm10, e.timestamp, e.gps});

                    writer.writeAll(data);
                    writer.close();

                    DownloadManager downloadManager = (DownloadManager) getActivity().getSystemService(DOWNLOAD_SERVICE);
                    File file = new File(csv);
                    downloadManager.addCompletedDownload(file.getName(), file.getName(), true, "text/plain",file.getAbsolutePath(),file.length(),true);
                } catch(IOException e) {
                    e.printStackTrace();
                }
            }
        });

        loadDb();
    }

    void loadDb() {
        ParticleDBHelper db = new ParticleDBHelper(getActivity());
        mListAdapter.clear();
        mListAdapter.addAll(db.getAllMesurements());
    }
}