package com.example.particle.ui.main;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.particle.MainActivity;
import com.example.particle.R;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

/**
 * A placeholder fragment containing a simple view.
 */
public class Connect extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private Button mStatusButton;
    private TextView mDeviceName;
    private Handler mHandler; // Our main handler that will receive callback notifications

    private BluetoothAdapter mBTAdapter;
    private BluetoothSocket mBTSocket;
    private LinearLayout mCurrentDevicePanel;
    private Set<BluetoothDevice> mPairedDevices;

    private ArrayAdapter<String> mBTArrayAdapter;
    private ListView mDevicesListView;
    private SharedPreferences mSettingsReader;
    private SharedPreferences.Editor mSettingsEditor;

    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"); // "random" unique identifier

    // #defines for identifying shared types between calling functions
    private final static int CONNECTING_STATUS = 1; // used in bluetooth handler to identify message status

    private PageViewModel pageViewModel;

    // ...
    // Define the listener of the interface type
    // listener will the activity instance containing fragment
    private ConnectionListener listener;

    // Define the events that the fragment will use to communicate
    public interface ConnectionListener {
        // This can be any number of events to be sent to the activity
        public void onConnected (BluetoothSocket socket);
        public void onDisconnected ();
    }

    public static Connect newInstance(int index) {
        Connect fragment = new Connect();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    // This event fires 1st, before creation of fragment or any views
    // The onAttach method is called when the Fragment instance is associated with an Activity.
    // This does not mean the Activity is fully initialized.
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity){
            this.listener = (ConnectionListener) context;
            mBTArrayAdapter = new ArrayAdapter<String>(context,android.R.layout.simple_list_item_1);
            mBTAdapter = BluetoothAdapter.getDefaultAdapter(); // get a handle on the bluetooth radio
            if(!mBTAdapter.isEnabled()) {
                startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), 1);
            }

            mHandler = new Handler(){
                public void handleMessage(android.os.Message msg){
                    if(msg.what == CONNECTING_STATUS){
                        if(msg.arg1 == 1) {
                            setStatusButtonConnected();
                            listener.onConnected(mBTSocket);
                        } else {
                            setStatusButtonDisconnected();
                            listener.onDisconnected();
                            Toast.makeText(getActivity(), (String)msg.obj, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            };
            //get access to saved settings
            mSettingsReader = getActivity().getSharedPreferences("ParticlePrefs", 0); // 0 - for private mode
            mSettingsEditor = mSettingsReader.edit();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        pageViewModel.setIndex(index);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_connect, container, false);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mStatusButton = (Button)view.findViewById(R.id.buttonStatus);
        mDevicesListView = (ListView)view.findViewById(R.id.dataList);
        mCurrentDevicePanel = (LinearLayout) view.findViewById(R.id.currentDevicePanel);
        mDeviceName = (TextView)view.findViewById(R.id.deviceName);

        mDevicesListView.setAdapter(mBTArrayAdapter); // assign model to view
        mDevicesListView.setOnItemClickListener(mDeviceClickListener);

        if (mBTAdapter == null) {
            // Device does not support Bluetooth
            mStatusButton.setBackgroundColor(Color.RED);
            mStatusButton.setText("Status: Bluetooth not found");
        }
        else {
            listPairedDevices();
        }

        setStatusButtonDisconnected();

        if(mBTSocket != null && mBTSocket.isConnected()) {
            mHandler.obtainMessage(CONNECTING_STATUS, 1, -1)
                    .sendToTarget();
        } else {
            mHandler.obtainMessage(CONNECTING_STATUS, -1, -1, "Socket error")
                    .sendToTarget();
        }

        updateCurrentDevicePanel();
    }

    private void setStatusButtonConnected() {
        mStatusButton.setText("Disconnect");
        mStatusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Disconnect();
            }
        });
    }

    private void setStatusButtonDisconnected() {
        mStatusButton.setText("Connect");
        mStatusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                String mac = mSettingsReader.getString("Device_MAC", "");
                connectByMAC(mac);
            }
        });
    }

    private void connectByMAC(final String macAddr) {
        mStatusButton.setText("Connecting...");

        // Spawn a new thread to avoid blocking the GUI one
        new Thread()
        {
            public void run() {
                boolean fail = false;

                BluetoothDevice device = mBTAdapter.getRemoteDevice(macAddr);

                try {
                    mBTSocket = createBluetoothSocket(device);
                } catch (IOException e) {
                    fail = true;
                    mHandler.obtainMessage(CONNECTING_STATUS, -1, 1, "Bluetooth is disabled")
                            .sendToTarget();
                    return;
                }
                // Establish the Bluetooth socket connection.
                try {
                    mBTSocket.connect();
                } catch (IOException e) {
                    try {
                        fail = true;
                        mBTSocket.close();
                    } catch (IOException e2) {

                    }
                    mHandler.obtainMessage(CONNECTING_STATUS, -1, 1, "Socket failed to connect")
                            .sendToTarget();
                }
                if(fail == false) {
                    mHandler.obtainMessage(CONNECTING_STATUS, 1, -1)
                            .sendToTarget();
                }
            }
        }.start();
    }

    public void Disconnect() {
        //socket error
        try {
            mBTSocket.close();
            mHandler.obtainMessage(CONNECTING_STATUS, -1, -1, "Disconnected")
                    .sendToTarget();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateCurrentDevicePanel() {
        String name = mSettingsReader.getString("Device_Name", "");
        if(name != "") {
            mCurrentDevicePanel.setVisibility(View.VISIBLE);
            mDeviceName.setText(name);
        } else {
            mCurrentDevicePanel.setVisibility(View.INVISIBLE);
        }
    }

    final BroadcastReceiver blReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(BluetoothDevice.ACTION_FOUND.equals(action)){
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // add the name to the list
                mBTArrayAdapter.add(device.getName() + "\n" + device.getAddress());
                mBTArrayAdapter.notifyDataSetChanged();
            }
        }
    };

    private void listPairedDevices(){
        mBTArrayAdapter.clear();
        mPairedDevices = mBTAdapter.getBondedDevices();
        if(mBTAdapter.isEnabled()) {
            // put it's one to the adapter
            for (BluetoothDevice device : mPairedDevices)
                mBTArrayAdapter.add(device.getName() + "\n" + device.getAddress());
        }
        else
            Toast.makeText(getActivity(), "Bluetooth not on", Toast.LENGTH_SHORT).show();
    }

    private AdapterView.OnItemClickListener mDeviceClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3) {

            if(!mBTAdapter.isEnabled()) {
                Toast.makeText(getActivity(), "Bluetooth not on", Toast.LENGTH_SHORT).show();
                return;
            }

            // Get the device MAC address, which is the last 17 chars in the View
            String info = ((TextView) v).getText().toString();
            final String address = info.substring(info.length() - 17);
            final String name = info.substring(0,info.length() - 17);

            mSettingsEditor.putString("Device_MAC", address);
            mSettingsEditor.putString("Device_Name", name);

            mSettingsEditor.commit();

            updateCurrentDevicePanel();
        }
    };

    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {
        return  device.createRfcommSocketToServiceRecord(BTMODULEUUID);
    }
}