package com.example.particle.ui.main;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.particle.DataDB;
import com.example.particle.MainActivity;
import com.example.particle.ParticleDBHelper;
import com.example.particle.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.max;
import static java.lang.Integer.parseInt;
import static java.lang.Long.parseLong;
import static java.lang.Math.min;

/**
 * A placeholder fragment containing a simple view.
 */
public class Map extends Fragment implements OnMapReadyCallback {

    private static final String ARG_SECTION_NUMBER = "section_number";

    // #defines for identifying shared types between calling functions
    private final static int SENSOR_STATUS = 2; // used in bluetooth handler to identify message status

    private PageViewModel pageViewModel;

    private MapView mMapView;
    private GoogleMap mGoogleMap;

    public static Map newInstance(int index) {
        Map fragment = new Map();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    public void updateLocation(Location location) {
        LatLng l = new LatLng(location.getLatitude(), location.getLongitude());
        if(mGoogleMap != null)
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(l));
    }

    // This event fires 1st, before creation of fragment or any views
    // The onAttach method is called when the Fragment instance is associated with an Activity.
    // This does not mean the Activity is fully initialized.
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        pageViewModel.setIndex(index);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mMapView = (MapView)view.findViewById(R.id.mapView);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_map, container, false);

        mMapView = (MapView) v.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this); //this is important

        return v;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
        refreshMap();
    }

    void addLocation(double pm25, double pm10, LatLng location) {
        if(mGoogleMap != null) {
            CircleOptions m = new CircleOptions();
            m.radius(10);
            m.center(location);
            m.clickable(false);
            int value = (int) pm10;
            int green = 255 - min(value, 255);
            int red = 255 - green;
            m.fillColor(Color.rgb(red, green, 0));
            m.strokeColor(Color.rgb(red, green, 0));
            mGoogleMap.addCircle(m);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    void refreshMap() {
        mGoogleMap.clear();
        ParticleDBHelper db = new ParticleDBHelper(getActivity());
        LatLng lastLng = null;
        for(DataDB mes : db.getAllMesurements()) {
            String[] lesplit = mes.gps.split(":");
            if(lesplit.length == 2) {
                double lat = parseDouble(lesplit[0]);
                double lon = parseDouble(lesplit[1]);
                lastLng = new LatLng(lat, lon);
                try {
                    //try and add the location
                    addLocation(parseDouble(mes.pm25), parseDouble(mes.pm10), lastLng);
                } catch (Exception e) {}
            }

        }
        if(lastLng != null)
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lastLng, 16));
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}