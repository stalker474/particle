package com.example.particle.ui.main;

import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.particle.DataDB;
import com.example.particle.MainActivity;
import com.example.particle.ParticleDBHelper;
import com.example.particle.R;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class Sensor extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    // #defines for identifying shared types between calling functions
    private final static int SENSOR_STATUS = 2; // used in bluetooth handler to identify message status

    private TextView mPM25;
    private TextView mPM10;
    private Location mCurrentLocation = null;
    private int mSpeed = 3000;
    private int mAntiSpeed = 10;

    private Handler mHandler; // Our main handler that will receive callback notifications
    private Sensor.ConnectedThread mConnectedThread; // bluetooth background worker thread to send and receive data

    private PageViewModel pageViewModel;

    private DisconnectionListener listener;

    public void updateLocation(Location location) {
        mCurrentLocation = location;
    }

    // Define the events that the fragment will use to communicate
    public interface DisconnectionListener {
        // This can be any number of events to be sent to the activity
        public void onError();

        public void onSensor(boolean IsOk);
    }

    public static Sensor newInstance(int index) {
        Sensor fragment = new Sensor();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    // This event fires 1st, before creation of fragment or any views
    // The onAttach method is called when the Fragment instance is associated with an Activity.
    // This does not mean the Activity is fully initialized.
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            this.listener = (DisconnectionListener) context;

            mHandler = new Handler() {
                @RequiresApi(api = Build.VERSION_CODES.P)
                public void handleMessage(android.os.Message msg) {
                    if (msg.what == SENSOR_STATUS) {
                        if (msg.arg1 == 1) {
                            listener.onSensor(true);
                            List<String> strList = (List<String>) msg.obj;
                            mPM10.setText(strList.get(0));
                            mPM25.setText(strList.get(1));
                            mAntiSpeed--;
                            if(mAntiSpeed == 0) {
                                mAntiSpeed = 10;
                                writeData(strList.get(0), strList.get(1));
                            }
                        } else {
                            listener.onSensor(false);
                        }
                    }
                }
            };


        }
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    private void writeData(String pm25, String pm10) {
        ParticleDBHelper db = new ParticleDBHelper(getActivity());
        DataDB d = new DataDB();
        d.pm10 = pm10;
        d.pm25 = pm25;
        Long tsLong = System.currentTimeMillis();
        d.timestamp = tsLong.toString();
        if(mCurrentLocation != null) {
            long diff = tsLong - mCurrentLocation.getTime();
            if(diff < 60000) //less than 1 min
                d.gps = mCurrentLocation.getLatitude() + ":" + mCurrentLocation.getLongitude();
            else
                d.gps = "na";
        } else
            d.gps = "na";


        db.addData(d);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        pageViewModel.setIndex(index);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_sensor, container, false);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPM25 = (TextView)view.findViewById(R.id.textPM25);
        mPM10 = (TextView)view.findViewById(R.id.textPM10);
    }

    public void Begin(BluetoothSocket socket) {
        mConnectedThread = new ConnectedThread(socket);
        mConnectedThread.start();
    }

    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        public void run() {
            byte[] buffer = new byte[1024];  // buffer store for the stream
            int bytes; // bytes returned from read()
            // Keep listening to the InputStream until an exception occurs
            while (true) {
                write("fetch");
                SystemClock.sleep(mSpeed);
                try {
                    // Read from the InputStream
                    bytes = mmInStream.available();
                    if(bytes != 0) {
                        buffer = new byte[1024];
                        //SystemClock.sleep(100); //pause and wait for rest of data. Adjust this depending on your sending speed.
                        bytes = mmInStream.available(); // how many bytes are ready to be read?
                        bytes = mmInStream.read(buffer, 0, bytes); // record how many bytes we actually read
                        byte [] data = new byte[bytes];
                        System.arraycopy(buffer, 0, data, 0, bytes);
                        String s = new String(data, StandardCharsets.UTF_8);
                        List<String> items = Arrays.asList(s.split("\\s*:\\s*"));
                        if(items.size() == 2) {
                            mHandler.obtainMessage(SENSOR_STATUS, 1, -1, items)
                                    .sendToTarget();
                        } else {
                            //error
                            mHandler.obtainMessage(SENSOR_STATUS, -1, -1)
                                    .sendToTarget();
                        }
                    }
                } catch (IOException e) {
                    listener.onError();
                    break;
                }
            }
        }

        /* Call this from the main activity to send data to the remote device */
        public void write(String input) {
            byte[] bytes = input.getBytes();           //converts entered String into bytes
            try {
                mmOutStream.write(bytes);
            } catch (IOException e) { }
        }
    }
}