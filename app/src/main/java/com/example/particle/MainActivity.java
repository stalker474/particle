package com.example.particle;

import android.Manifest;
import android.annotation.SuppressLint;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.particle.ui.main.Connect;
import com.example.particle.ui.main.Data;
import com.example.particle.ui.main.Map;
import com.example.particle.ui.main.Sensor;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements Connect.ConnectionListener, Sensor.DisconnectionListener, LocationListener {

    private static final int GPS_ENABLE_REQUEST = 3;
    private static final int GPS_PERMISSION_REQUEST = 1;
    // GUI Components
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private Sensor mSensorFragment;
    private Connect mConnectFragment;
    private Data mDataFragment;
    private Map mMapFragment;
    private FusedLocationProviderClient mFusedLocationClient;
    private CheckBox mSensorCheck;
    private CheckBox mGpsCheck;
    private CheckBox mDeviceCheck;
    private AlertDialog mGPSDialog;
    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;

    private final String TAG = MainActivity.class.getSimpleName();

    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        setViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        mSensorCheck = (CheckBox) findViewById(R.id.sensorCheck);
        mGpsCheck = (CheckBox) findViewById(R.id.gpsCheck);
        mDeviceCheck = (CheckBox) findViewById(R.id.deviceCheck);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        resumeTracking();
    }

    private void resumeTracking() {
        //check if we have permission to track
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                /*|| ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED*/) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION/*, Manifest.permission.ACCESS_BACKGROUND_LOCATION*/}, GPS_PERMISSION_REQUEST);
        } else {
            mLocationRequest = LocationRequest.create();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setInterval(20 * 1000);
            mLocationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    if (locationResult == null) {
                        return;
                    }
                    for (Location location : locationResult.getLocations()) {
                        if (location != null) {
                            onLocationChanged(location);
                        }
                    }
                }
            };
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
            //force update now
            mFusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    // Got last known location. In some rare situations this can be null.
                    if (location != null) {
                        onLocationChanged(location);
                    }
                }
            });
            mGpsCheck.setChecked(true);
        }
    }

    private void stopTracking() {

        if (mFusedLocationClient != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
        mGpsCheck.setChecked(false);
    }

    public void showGPSDisabledDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("GPS Disabled");
        builder.setMessage("Gps is disabled, in order to use the application properly you need to enable GPS of your device");
        builder.setPositiveButton("Enable GPS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), GPS_ENABLE_REQUEST);
            }
        }).setNegativeButton("No, Just Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        mGPSDialog = builder.create();
        mGPSDialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case GPS_PERMISSION_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //try again
                    resumeTracking();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void setViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        mSensorFragment = new Sensor();
        mConnectFragment = new Connect();
        mDataFragment = new Data();
        mMapFragment = new Map();
        adapter.addFragment(mConnectFragment, "Connect");
        adapter.addFragment(mSensorFragment, "Sensor");
        adapter.addFragment(mDataFragment, "Database");
        adapter.addFragment(mMapFragment, "Map");
        viewPager.setAdapter(adapter);
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    public void onConnected(BluetoothSocket socket) {
        mSensorFragment.Begin(socket);
        mDeviceCheck.setChecked(true);
        //try and track location
        resumeTracking();
    }

    @Override
    public void onDisconnected() {
        mDeviceCheck.setChecked(false);
        stopTracking();
    }

    @Override
    public void onError() {
        mConnectFragment.Disconnect();
        mDeviceCheck.setChecked(false);
    }

    @Override
    public void onSensor(boolean IsOk) {
        mSensorCheck.setChecked(IsOk);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if(location != null) {
            mSensorFragment.updateLocation(location);
            mMapFragment.updateLocation(location);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mGpsCheck.setChecked(status != LocationProvider.AVAILABLE);
        } else {

        }
    }

    @Override
    public void onProviderEnabled(String provider) {
        if (provider.equals(LocationManager.GPS_PROVIDER)) {
            resumeTracking();
        }
    }

    @Override
    public void onProviderDisabled(String provider) {
        if (provider.equals(LocationManager.GPS_PROVIDER))
        {
            stopTracking();
            showGPSDisabledDialog();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GPS_ENABLE_REQUEST && (resultCode == PackageManager.PERMISSION_GRANTED))
        {
            resumeTracking();
        }
    }
}
