package com.example.particle;

import java.io.Serializable;
import java.util.Date;
import static java.lang.Long.parseLong;

public class DataDB implements Serializable {
    public String pm25;
    public String pm10;
    public String gps;
    public String timestamp;

    @Override
    public String toString() {
        String date = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(parseLong(this.timestamp));
        String res = date + " " + this.pm25 + " " + this.pm10;
        if(this.gps.compareTo("na") != 0)
            res += " with location";
        return res;
    }
}